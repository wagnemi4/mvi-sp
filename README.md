# MVI - Semestral work


## Topic - Trading at close
https://www.kaggle.com/competitions/optiver-trading-at-the-close/overview

In this competition, you are challenged to develop a model capable of predicting the closing price movements for hundreds of Nasdaq listed stocks using data from the order book and the closing auction of the stock. Information from the auction can be used to adjust prices, assess supply and demand dynamics, and identify trading opportunities.

## Data source 
https://www.kaggle.com/competitions/optiver-trading-at-the-close/data
